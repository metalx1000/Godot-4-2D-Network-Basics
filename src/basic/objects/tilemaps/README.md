
# godot-objects

Copyright Kris Occhipinti 2022-02-14

(https://filmsbykris.com)

License GPLv3

# Install 
- Run the following to install
```
sudo wget "https://gitlab.com/metalx1000/godot-objects/-/raw/master/godot-objects.sh" -O /usr/local/bin/godot-objects
sudo chmod +x /usr/local/bin/godot-objects
```
