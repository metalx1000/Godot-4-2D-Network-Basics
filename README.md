# Godot-4-2D-Network-Basics

Copyright Kris Occhipinti 2023-04-06

(https://filmsbykris.com)

License GPLv3

To start server
~~~
godot4 --path src/basic --headless --server
~~~

To connect to server (currently localhost)
~~~
godot4 --path src/basic
~~~
